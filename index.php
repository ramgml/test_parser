<?php
/**
 * Created by PhpStorm.
 * User: yutuk
 * Date: 12/16/2018
 * Time: 2:59 PM
 */

require_once './vendor/autoload.php';
//Получаем настройки для базы данных
$settings = parse_ini_file('./settings.ini', 1);

$db = new \Picom\Database(
    $settings['db']['host'],
    $settings['db']['dbname'],
    $settings['db']['user'],
    $settings['db']['password']
);
//Подключаемся к базе
$db->init();

$dom = new DiDom\Document();
$baseUri = 'https://www.td-kama.com';
//Загружаем страницу с новостями
$dom->loadHTMLFile($baseUri.'/ru/main/news/');
//Получаем все элементы с классом news-item
$elements = $dom->find('ul.news-list>li.news-item');
//Инициализируем итератор, который будет фильтровать новости за последний месяц
$iterator = new \Picom\ElementsIterator($elements);
//Устанавливаем селектор для получения даты новости
$iterator->setDateSelector('.news-item_summary>p');
//Запускаем итерирование новостей
foreach ($iterator as $key => $li) {
    try {
        $parser = new \Picom\Parser($li);
        $item = [];
        $item['title']
            = $parser->getText('.news-item_summary>.news-item_text>a');
        $item['image'] = $parser->getAttribute(
            '.news-item_image>img',
            'data-src'
        );
        $item['link'] = $parser->getAttribute('.news-item_link>a', 'href');
        //Загружаем страницу новости
        $dom->loadHTMLFile($baseUri.$item['link']);
        //Заменяем ноду от которой ищем данные
        $parser->changeElement($dom->first('main'));

        $item['date'] = \Picom\Helper::formatDate(
            $parser->getText('div.text-grey')
        );
        $item['intro'] = $parser->getText('article>p');
        //Получаем id из ссылки на страницу новости,
        //по нему будет определяться ункальность новости в базе данных
        $item['bx_id'] = \Picom\Helper::getBxIdFromUri($item['link']);
        //Добавляем или обновляем новость в базе данных
        $db->upsert($item);
    } catch (Exception $exception) {
        //Ловим исключение, но продолжаем перебор элементов
        printf(
            "%s: %s \n",
            $key,
            $exception->getMessage()
        );
    }
}

