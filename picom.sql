-- Dumping structure for table picom.news
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `image` varchar(500) NOT NULL,
  `link` varchar(255) NOT NULL,
  `intro` text NOT NULL,
  `bx_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `news__ux` (`bx_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
