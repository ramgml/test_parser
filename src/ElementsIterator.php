<?php
/**
 * Created by PhpStorm.
 * User: yutuk
 * Date: 12/17/2018
 * Time: 12:16 PM
 */

namespace Picom;


use DiDom\Element;

class ElementsIterator implements \Iterator
{
    /**
     * @var Element[]
     */
    private $elements;

    private $position;

    private $perviousElementMonth;

    private $dateSelector;

    private $currentMonth;

    /**
     * ElementsIterator constructor.
     *
     * @param array $elements
     */
    public function __construct(array $elements)
    {
        $this->elements = $elements;
        $this->position = 0;
    }

    /**
     * Return the current element
     *
     * @link  http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current()
    {
        return $this->elements[$this->position];
    }

    /**
     * Move forward to next element
     *
     * @link  http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        ++$this->position;
    }

    /**
     * Return the key of the current element
     *
     * @link  http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * Checks if current position is valid
     *
     * @link  http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then
     *                 evaluated. Returns true on success or false on failure.
     * @since 5.0.0
     * @throws \Exception
     */
    public function valid()
    {
        if (empty($this->dateSelector)) {
            throw new \Exception('The date selector must be set');
        }
        if (isset($this->elements[$this->position])) {
            $el = $this->elements[$this->position];
            $currentDate = $el->first($this->dateSelector)->text();
            $currentMonth = Helper::getMonthFromString($currentDate);

            $isTheSameMonth = !$this->perviousElementMonth
                || $currentMonth === $this->perviousElementMonth;

            $this->perviousElementMonth = $currentMonth;

            return $isTheSameMonth;
        }

        return false;
    }

    /**
     * Rewind the Iterator to the first element
     *
     * @link  http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        $this->position = 0;
    }

    public function setDateSelector(string $selector)
    {
        $this->dateSelector = $selector;
    }
}