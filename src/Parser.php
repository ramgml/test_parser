<?php
/**
 * Created by PhpStorm.
 * User: yutuk
 * Date: 12/17/2018
 * Time: 2:40 PM
 */

namespace Picom;


use DiDom\Element;

class Parser
{
    private $el;

    public function __construct(Element $el)
    {
        $this->el = $el;
    }

    public function changeElement(Element $el)
    {
        $this->el = $el;
    }

    public function getText(string $selector)
    {
        $text = $this->el->first($selector)->text();

        return trim($text);
    }

    public function getAttribute(string $selector, string $attr)
    {
        $attr = $this->el->first($selector)->getAttribute($attr);

        return trim($attr);
    }
}