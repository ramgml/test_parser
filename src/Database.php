<?php
/**
 * Created by PhpStorm.
 * User: yutuk
 * Date: 12/17/2018
 * Time: 1:56 PM
 */

namespace Picom;

use PDO;

class Database
{
    /**
     * @var PDO $pdo
     */
    private $pdo;
    /**
     * @var string $host
     */
    private $host;
    /**
     * @var string $dbname
     */
    private $dbname;
    /**
     * @var string $username
     */
    private $username;
    /**
     * @var string $password
     */
    private $password;

    /**
     * Database constructor.
     *
     * @param $host
     * @param $dbname
     * @param $username
     * @param $password
     */
    public function __construct($host, $dbname, $username, $password)
    {
        $this->host = $host;
        $this->dbname = $dbname;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Init connection
     */
    public function init()
    {
        $dsn = "mysql:host=".$this->host
            .";dbname=".$this->dbname
            .";charset=utf8";

        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        ];
        $this->pdo = new PDO(
            $dsn,
            $this->username,
            $this->password,
            $opt
        );
    }

    /**
     * Insert or update a row in the table news
     *
     * @param array $item
     */
    public function upsert($item)
    {
        $sql
            = "INSERT INTO news (title, date, image, link, intro, bx_id) 
                VALUES (:title, :date, :image, :link, :intro, :bx_id) 
                ON DUPLICATE KEY UPDATE title=:title, date=:date, image=:image,
                link=:link, intro=:intro";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($item);
    }
}