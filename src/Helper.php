<?php
/**
 * Created by PhpStorm.
 * User: yutuk
 * Date: 12/16/2018
 * Time: 5:23 PM
 */

namespace Picom;


class Helper
{
    /**
     * @param string $date
     *
     * @return string
     */
    public static function getMonthFromString(string $date)
    {
        $dateArray = explode(' ', $date);
        $low = mb_convert_case($dateArray[1], MB_CASE_LOWER, "UTF-8");

        return trim($low);
    }

    /**
     * @param string $uri
     *
     * @return mixed
     */
    public static function getBxIdFromUri(string $uri)
    {
        $arr = explode('/', $uri);
        $filtred = array_filter($arr);

        return end($filtred);
    }

    public static function formatDate(string $date)
    {
        return date(
            'Y-m-d',
            strtotime($date)
        );
    }
}